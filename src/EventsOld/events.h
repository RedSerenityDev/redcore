#ifndef REDCORE_EVENTS_H
#define REDCORE_EVENTS_H

#include "php.h"

void MINIT_EventsClassInterface();

ZEND_BEGIN_ARG_INFO(Void_ArgInfo, 0)
ZEND_END_ARG_INFO();

ZEND_BEGIN_ARG_INFO(Watcher_Event_ArgInfo, 0)
		ZEND_ARG_OBJ_INFO(0, Event, RedSerenity\\Core\\Events\\Event, 0)
ZEND_END_ARG_INFO();

PHP_METHOD(Manager, __construct);
PHP_METHOD(Manager, Fire);
PHP_METHOD(Manager, Register);
PHP_METHOD(Manager, Unregister);
PHP_METHOD(Manager, Listeners);

static const zend_function_entry RsCore_Events_EventInterface_Functions[] = {
	ZEND_FE_END
};

static const zend_function_entry RsCore_Events_WatcherInterface_Functions[] = {
	ZEND_ABSTRACT_ME(Watcher, __onEvent, Watcher_Event_ArgInfo)
	ZEND_FE_END
};

static const zend_function_entry RsCore_Events_ManagerClass_Functions[] = {
	ZEND_ME(Manager, __construct, Void_ArgInfo, ZEND_ACC_PUBLIC)
	ZEND_ME(Manager, Fire, Watcher_Event_ArgInfo, ZEND_ACC_PUBLIC)
	ZEND_ME(Manager, Listeners, Void_ArgInfo, ZEND_ACC_PUBLIC)
	ZEND_FE_END
};

extern zend_class_entry *RsCore_Events_EventInterface_ce;
extern zend_class_entry *RsCore_Events_WatcherInterface_ce;
extern zend_class_entry *RsCore_Events_ManagerClass_ce;

#endif //REDCORE_EVENTS_H

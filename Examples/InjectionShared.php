<?php

namespace {

	class MySharedService implements \RedSerenity\Core\Service {

		static protected $Random = NULL;

		public function __construct() {
			if (!self::$Random) {
				self::$Random = "Hello #" . rand(1000,10000);
			}
		}

		public function SayHello() {
			echo self::$Random . PHP_EOL;
		}

	}

	class Something extends \RedSerenity\Core\Injection {

		public function __construct() {

			$this->MyService->SayHello();

		}

		public function __inject(MySharedService $MyService) {}

	}


	echo "The following two numbers should be the same." . PHP_EOL;
	$Something = new Something();
	$SomethingAgain = new Something();

}
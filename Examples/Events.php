<?php

namespace {

	class MyEvent implements \RedSerenity\Core\Events\Event {

		public $Data;

		public function __construct(string $ArbitraryData) {
			$this->Data = $ArbitraryData;
		}

	}

	class MyWatcher implements \RedSerenity\Core\Events\Watcher {

		const PRIORITY = \RedSerenity\Core\Events\Type::PRIORITY_NORMAL;

		public function __onEvent(\RedSerenity\Core\Events\Event $Event) {
			echo $Event->Data;
		}

	}

	class MyCoolClass implements \RedSerenity\Core\Injectable {

		public function __inject(\RedSerenity\Core\Events\Manager $EventSystem) {}

		public function __construct() {

			$this->EventSystem->Fire(new MyEvent("Hello, World!"));

		}

	}


	\RedSerenity\Core\EventSystem::Watch(MyWatcher::class, MyEvent::class);

	$T = new MyCoolClass();

}
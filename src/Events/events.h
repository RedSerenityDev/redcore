#ifndef REDCORE_EVENTS_H
#define REDCORE_EVENTS_H

#include "redcore.h"
#include "Zend/zend_exceptions.h"
#include "ext/spl/spl_exceptions.h"

#define EventException(Message, ...) zend_throw_exception_ex(spl_ce_RuntimeException, 0, Message, ##__VA_ARGS__)

void MINIT_EventHooks();
void MSHUTDOWN_EventHooks();

PHP_METHOD(Events, RegisterEvent);
PHP_METHOD(Events, UnregisterEvent);
PHP_METHOD(Events, RegisteredEvents);
ZEND_METHOD(Events, __EVENT__);

ZEND_BEGIN_ARG_INFO(Events_Void_ArgInfo, 0)
ZEND_END_ARG_INFO();

ZEND_BEGIN_ARG_INFO(Events_Hook_ArgInfo, 0)
	ZEND_ARG_TYPE_INFO(0, OriginalClass, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, EventClass, IS_STRING, 0)
ZEND_END_ARG_INFO();

ZEND_BEGIN_ARG_INFO(Events_Hook_BeforeInterface_ArgInfo, 0)
		ZEND_ARG_INFO(0, OriginalClass)
		ZEND_ARG_TYPE_INFO(0, Method, IS_STRING, 0)
		ZEND_ARG_ARRAY_INFO(0, Params, 1)
ZEND_END_ARG_INFO();

ZEND_BEGIN_ARG_INFO(Events_Hook_AfterInterface_ArgInfo, 0)
		ZEND_ARG_INFO(0, OriginalClass)
		ZEND_ARG_TYPE_INFO(0, Method, IS_STRING, 0)
		ZEND_ARG_TYPE_INFO(0, ReturnValue, 0, 1)
ZEND_END_ARG_INFO();

static const zend_function_entry RsCore_EventClass_Functions[] = {
	PHP_ME(Events, RegisterEvent, Events_Hook_ArgInfo, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(Events, UnregisterEvent, Events_Hook_ArgInfo, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(Events, RegisteredEvents, Events_Void_ArgInfo, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	ZEND_ME(Events, __EVENT__, Events_Void_ArgInfo, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	ZEND_FE_END
};

static const zend_function_entry RsCore_OnEventInterface_Functions[] = {
	ZEND_ABSTRACT_ME(OnEvent, __BeforeEvent, Events_Hook_BeforeInterface_ArgInfo)
	ZEND_ABSTRACT_ME(OnEvent, __AfterEvent, Events_Hook_AfterInterface_ArgInfo)
	ZEND_FE_END
};

extern zend_class_entry *RsCore_EventClass_ce;
extern zend_class_entry *RsCore_OnEventInterface_ce;

#endif //REDCORE_EVENTS_H

#ifndef REDCORE_SERVICE_H
#define REDCORE_SERVICE_H

#include "redcore.h"

void MINIT_ServiceClassInterface();

ZEND_BEGIN_ARG_INFO(Void_ArgInfo, 0)
ZEND_END_ARG_INFO();

PHP_METHOD(SharedService, __construct);
PHP_METHOD(SharedService, __clone);
PHP_METHOD(SharedService, __wakeup);
PHP_METHOD(SharedService, __instance);

static const zend_function_entry RsCore_ServiceInterface_Functions[] = {
	ZEND_FE_END
};

static const zend_function_entry RsCore_SharedServiceClass_Functions[] = {
	ZEND_ME(SharedService, __construct, Void_ArgInfo, ZEND_ACC_PROTECTED)
	ZEND_ME(SharedService, __clone, Void_ArgInfo, ZEND_ACC_PRIVATE)
	ZEND_ME(SharedService, __wakeup, Void_ArgInfo, ZEND_ACC_PRIVATE)
	ZEND_ME(SharedService, __instance, Void_ArgInfo, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	ZEND_FE_END
};

extern zend_class_entry *RsCore_ServiceInterface_ce;
extern zend_class_entry *RsCore_SharedServiceClass_ce;

#endif //REDCORE_SERVICE_H

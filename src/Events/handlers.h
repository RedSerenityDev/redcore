#ifndef REDCORE_HANDLERS_H
#define REDCORE_HANDLERS_H

#include "redcore.h"

#define BEFORE_EVENT "__beforeevent"
#define AFTER_EVENT "__afterevent"
#define TEST_EVENT "__test"

void OverloadOpcodeHandlers();
void RestoreOpcodeHandlers();

ZEND_API zval* red_call_method(zval *object, zend_class_entry *obj_ce, zend_function **fn_proxy, const char *function_name, size_t function_name_len, zval *retval_ptr, int param_count, zval* arg1, zval* arg2, zval* arg3);


#endif //REDCORE_HANDLERS_H

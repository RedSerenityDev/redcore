dnl $Id$
dnl config.m4 for extension redcore

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(redcore, for redcore support,
dnl Make sure that the comment is aligned:
dnl [  --with-redcore             Include redcore support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(redcore, whether to enable redcore support, [  --enable-redcore           Enable RedCore support])

if test "$PHP_REDCORE" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-redcore -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/redcore.h"  # you most likely want to change this
  dnl if test -r $PHP_REDCORE/$SEARCH_FOR; then # path given as parameter
  dnl   REDCORE_DIR=$PHP_REDCORE
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for redcore files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       REDCORE_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$REDCORE_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the redcore distribution])
  dnl fi

  dnl # --with-redcore -> add include path
  dnl PHP_ADD_INCLUDE($REDCORE_DIR/include)

  dnl # --with-redcore -> check for lib and symbol presence
  dnl LIBNAME=redcore # you may want to change this
  dnl LIBSYMBOL=redcore # you most likely want to change this

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $REDCORE_DIR/$PHP_LIBDIR, REDCORE_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_REDCORELIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong redcore lib version or lib not found])
  dnl ],[
  dnl   -L$REDCORE_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(REDCORE_SHARED_LIBADD)

  PHP_NEW_EXTENSION(redcore, redcore.c src/dummy/dummy.c src/helpers/helpers.c src/exception/base.c src/exception/core.c src/injectable/injectable.c src/service/service.c src/singleton/singleton.c src/events/events.c src/events/execute.c src/events/hooks.c src/events/handlers.c, $ext_shared,,)
fi

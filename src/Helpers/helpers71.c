//
// Created by Tyler Andersen on 10/23/17.
//

#ifndef REDCORE_HELPERS_71_H
#define REDCORE_HELPERS_71_H

#include "php.h"

static void Zend_Echo(zend_string *String) {

}

static void Zend_PrintF(zend_string *Format, ...) {
	//php_printf();
}

static zval *Zend_String_AsZval(zend_string *String) {
	zval *tmp;
	ZVAL_STR(tmp, String);
	return tmp;
}

static zend_string *Zend_String_AsString(zval *String) {
	return Z_STR_P(String);
}

static char *Zend_String_AsChar(zend_string *String) {
	return String->val;
}

static zend_string *Zend_String_New(const char *String) {
	return zend_string_init(String, strlen(String), 1);
}

static size_t Zend_String_Length(zend_string *String) {
	return String->len;
}

static zval *Zend_Object_AsZval(zend_object *Object) {
	zval *tmp;
	ZVAL_OBJ(tmp, Object);
	return tmp;
}

static zend_object *Zend_Object_AsObject(zval *Object) {
	return Z_OBJ_P(Object);
}

static zend_class_entry *Zend_Object_Ce(zend_object *Object) {
	return Object->ce;
}

static zend_function *Zend_Object_Handler(zend_object *Object, const char *HandlerName) {
	/* Will this cause a problem in C because HandlerName is dynamic? */
	/*return Object->handlers->(HandlerName);*/
}

static zend_object *Zend_Object_New(zend_class_entry *Ce, zend_object_handlers *Oh) {
	zend_object *RetVal;

	RetVal = zend_objects_new(Ce);
	if (!RetVal) {
		/* TODO: Throw real exception */
		zend_error(E_ERROR, "Could instantiate class.");
	}

	if (Oh) {
		RetVal->handlers = Oh;
	}
	object_properties_init(RetVal, Ce);

	return RetVal;
}

static zend_function *Zend_Object_GetConstructor(zend_object *Object) {
	return Object->handlers->get_constructor(Object);
}

static zend_function *Zend_Object_GetFunction(zend_object *Object, const char *FunctionName) {
	return Object->handlers->get_method(&Object, zend_string_init(FunctionName, sizeof(FunctionName)-1, 0), NULL);
}

static zval *Zend_Object_CallFunction(zend_function *Function, ...) {

}

static int Zend_Object_AddConstant(zend_class_entry *Ce, const char *ConstantName, zval *Value) {

}

static int Zend_Object_AddProperty(zval *Object, const char *PropertyName, zval *Value, int AccessLevel) {

}

static void Zend_Object_WriteProperty(zval *Object, const char *PropertyName, zval *Value) {

}

static zval *Zend_Object_ReadProperty(zval *Object, const char *PropertyName, zval *Value) {

}

static void Zend_Object_UnsetProperty(zval *Object, const char *PropertyName) {

}

static int Zend_Object_AddStaticProperty(zend_class_entry *Ce, const char *PropertyName, zval *Value, int AccessLevel) {

}

static void Zend_Object_WriteStaticProperty(zend_class_entry *Ce, const char *PropertyName, zval *Value) {

}

static zval *Zend_Object_ReadStaticProperty(zend_class_entry *Ce, const char *PropertyName, zval *Value) {

}

/*****************************/

typedef struct __Zend {

	void (*Echo)(zend_string *String);

	void (*PrintF)(zend_string *Format, ...);

	struct {
		zval *(*AsZval)(zend_string *String);

		zend_string *(*AsString)(zval *String);

		char *(*AsChar)(zend_string *String);

		zend_string *(*New)(const char *String);

		size_t (*Length)(zend_string *String);
	} String;

	struct {
		zval *(*AsZval)(zend_object *Object);

		zend_object *(*AsObject)(zval *Object);

		zend_class_entry *(*Ce)(zend_object *Object);

		zend_function *(*Handler)(zend_object *Object, const char *HandlerName);


		zend_object *(*New)(zend_class_entry *Ce, zend_object_handlers *Oh);

		zend_function *(*GetConstructor)(zend_object *Object);

		zend_function *(*GetFunction)(zend_object *Object, const char *FunctionName);

		zval *(*CallFunction)(zend_function *Function, ...);


		int (*AddConstant)(zend_class_entry *Ce, const char *ConstantName, zval *Value);


		int (*AddProperty)(zval *Object, const char *PropertyName, zval *Value, int AccessLevel);

		void (*WriteProperty)(zval *Object, const char *PropertyName, zval *Value);

		zval *(*ReadProperty)(zval *Object, const char *PropertyName, zval *Value);

		void (*UnsetProperty)(zval *Object, const char *PropertyName);


		int (*AddStaticProperty)(zend_class_entry *Ce, const char *PropertyName, zval *Value, int AccessLevel);

		void (*WriteStaticProperty)(zend_class_entry *Ce, const char *PropertyName, zval *Value);

		zval *(*ReadStaticProperty)(zend_class_entry *Ce, const char *PropertyName, zval *Value);
	} Object;

	struct {

	} Array;

} _Zend;

_Zend Zend() {
	_Zend ZendClass;

	ZendClass.Echo = Zend_Echo;
	ZendClass.PrintF = Zend_PrintF;

	ZendClass.String.AsZval = Zend_String_AsZval;
	ZendClass.String.AsString = Zend_String_AsString;
	ZendClass.String.AsChar = Zend_String_AsChar;
	ZendClass.String.New = Zend_String_New;
	ZendClass.String.Length = Zend_String_Length;

	ZendClass.Object.AsZval = Zend_Object_AsZval;
	ZendClass.Object.AsObject = Zend_Object_AsObject;
	ZendClass.Object.Ce = Zend_Object_Ce;
	ZendClass.Object.Handler = Zend_Object_Handler;
	ZendClass.Object.New = Zend_Object_New;
	ZendClass.Object.GetConstructor = Zend_Object_GetConstructor;
	ZendClass.Object.GetFunction = Zend_Object_GetFunction;
	ZendClass.Object.CallFunction = Zend_Object_CallFunction;
	ZendClass.Object.AddConstant = Zend_Object_AddConstant;
	ZendClass.Object.AddProperty = Zend_Object_AddProperty;
	ZendClass.Object.WriteProperty = Zend_Object_WriteProperty;
	ZendClass.Object.ReadProperty = Zend_Object_ReadProperty;
	ZendClass.Object.UnsetProperty = Zend_Object_UnsetProperty;
	ZendClass.Object.AddStaticProperty = Zend_Object_AddStaticProperty;
	ZendClass.Object.WriteStaticProperty = Zend_Object_WriteStaticProperty;
	ZendClass.Object.ReadStaticProperty = Zend_Object_ReadStaticProperty;

	return ZendClass;
}


#endif //REDCORE_HELPERS_71_H

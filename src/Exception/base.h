#ifndef REDCORE_EXCEPTION_BASE_H
#define REDCORE_EXCEPTION_BASE_H

#include "redcore.h"

void MINIT_ExceptionBase();

static const zend_function_entry RsCore_ExceptionBase_Functions[] = {
		ZEND_FE_END
};

extern zend_class_entry *RsCore_ExceptionBase_ce;

#endif //REDCORE_EXCEPTION_BASE_H

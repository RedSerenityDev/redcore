#ifndef REDCORE_EXCEPTION_CORE_H
#define REDCORE_EXCEPTION_CORE_H

#include "redcore.h"

void MINIT_ExceptionCore();

static const zend_function_entry RsCore_ExceptionCore_Functions[] = {
		ZEND_FE_END
};

extern zend_class_entry *RsCore_ExceptionCore_ce;

#endif //REDCORE_EXCEPTION_CORE_H

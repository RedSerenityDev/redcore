#include <zend_interfaces.h>
#include "php_redcore.h"
#include "redcore.h"
#include "injectable.h"
#include "../service/service.h"

zend_class_entry *RsCore_InjectableInterface_ce;

static void CallConstructor(zend_object *Object) {
	zend_function *Constructor;

	Constructor = Object->handlers->get_constructor(Object);
	if (Constructor) {
		zval retval;
		zval *params = NULL;
		int ret, i;
		zend_fcall_info fci;
		zend_fcall_info_cache fcc;

		fci.size = sizeof(fci);
		ZVAL_UNDEF(&fci.function_name);
		fci.object = Object;
		fci.retval = &retval;
		fci.param_count = 0;
		fci.params = params;
		fci.no_separation = 1;

		fcc.initialized = 1;
		fcc.function_handler = Constructor;
		fcc.calling_scope = zend_get_executed_scope();
		fcc.called_scope = Object->ce;
		fcc.object = Object;

		ret = zend_call_function(&fci, &fcc);
		if (ret == FAILURE) {
			php_error_docref(NULL, E_WARNING, "Invocation of %s's constructor failed", ZSTR_VAL(Object->ce->name));
		}

	}
}

/*static void CallInstance(zend_object *Object, zval *RetVal) {
	zend_function *Instance;
	zval rv;

	zend_string *InstanceName;
	InstanceName = zend_string_init("__Instance", sizeof("__Instance")-1, 0);
	Instance = Object->handlers->(&Object, InstanceName, &rv);
	zend_string_release(InstanceName);

	if (!Instance) {
		zend_error(E_ERROR, "Could not call %s::__instance().", ZSTR_VAL(Object->ce->name));
	}

	zval *params = NULL;
	int ret, i;
	zend_fcall_info fci;
	zend_fcall_info_cache fcc;

	fci.size = sizeof(fci);
	ZVAL_UNDEF(&fci.function_name);
	fci.object = Object;
	fci.retval = RetVal;
	fci.param_count = 0;
	fci.params = params;
	fci.no_separation = 1;

	fcc.initialized = 1;
	fcc.function_handler = Instance;
	fcc.calling_scope = zend_get_executed_scope();
	fcc.called_scope = Object->ce;
	fcc.object = Object;

	ret = zend_call_function(&fci, &fcc);
	if (ret == FAILURE) {
		php_error_docref(NULL, E_WARNING, "Could not call %s::__Instance()", ZSTR_VAL(Object->ce->name));
	}
}*/

static void SetProperty(zend_object *Object, zend_string *PropertyName, zval *Value) {
	/* This is working code! */
	/*zval tmp;
	ZVAL_STR(&tmp, zend_string_init("What the what", sizeof("What the what")-1, 0));
	SetProperty(Object, "test123", &tmp);
	zval_ptr_dtor(&tmp);*/

	zval ZObject;
	Z_OBJ(ZObject) = Object;
	zend_update_property_ex(Object->ce, &ZObject, PropertyName, Value);
	zval_ptr_dtor(&ZObject);
}

static void SetPropertyToClass(zend_object *Object, zend_string *PropertyName, zend_string *ClassName) {
	zval ZObject;
	Z_OBJ(ZObject) = Object;

	zend_class_entry *ArgCe;
	zend_object *ArgObj;

	ArgCe = zend_lookup_class(ClassName);
	if (!ArgCe) {
		zend_error(E_ERROR, "Could not lookup class %s specified in %s::__inject()", ZSTR_VAL(ClassName), ZSTR_VAL(Object->ce->name));
	}

	if (!instanceof_function(ArgCe, RsCore_ServiceInterface_ce)) {
		zend_error(E_ERROR, "Class %s does not implement RedSerenity\\Core\\Service. Only classes that implement RedSerenity\\Core\\Service can be used in __inject().", ZSTR_VAL(ArgCe->name));
	}

	if (instanceof_function(ArgCe, RsCore_SharedServiceClass_ce)) {

		zval InstanceRV;
		if(!zend_call_method(NULL, ArgCe, NULL, "__instance", sizeof("__instance")-1, &InstanceRV, 0, NULL, NULL)) {
			zend_error(E_ERROR, "Could not call %s::__instance() while trying to inject Service.", ZSTR_VAL(ArgCe->name));
		}
		zend_update_property_ex(Object->ce, &ZObject, PropertyName, &InstanceRV);
		zval_ptr_dtor(&InstanceRV);
	} else {
		ArgObj = zend_objects_new(ArgCe);
		if (!ArgObj) {
			zend_error(E_ERROR, "Could not instantiate class %s specified in %s::__inject()", ZSTR_VAL(ClassName), ZSTR_VAL(Object->ce->name));
		}

		object_properties_init(ArgObj, ArgCe);
		CallConstructor(ArgObj);

		zval Value;
		ZVAL_OBJ(&Value, ArgObj);
		zend_update_property_ex(Object->ce, &ZObject, PropertyName, &Value);
		zval_ptr_dtor(&Value);
	}
}

static zend_function *GetFunction(zend_object *Object, char *FunctionName) {
	zend_string *_FunctionName;
	_FunctionName = zend_string_init(FunctionName, strlen(FunctionName), 0);
	zend_function *RetVal = Object->handlers->get_method(&Object, _FunctionName, NULL);
	zend_string_release(_FunctionName);

	if (!RetVal) {
		zend_error(E_ERROR, "Could not get the %s method from the class %s.", FunctionName, ZSTR_VAL(Object->ce->name));
	}

	return RetVal;
}

static void ProcessInjectArguments(zend_function *InjectFunction, zend_object *Object) {
	zend_arg_info *Arguments;
	uint32_t i, ArgumentsCount;

	Arguments = InjectFunction->common.arg_info;
	ArgumentsCount = InjectFunction->common.num_args;

	for (i = 0; i < ArgumentsCount; i++) {
		if (!Arguments->class_name) {
			zend_error(E_ERROR, "Scalar TypeHints are not allowed for __inject() method in class %s. $%s as a(n) %s is not allowed.",
								 ZSTR_VAL(Object->ce->name),
								 ZSTR_VAL(Arguments->name),
								 zend_get_type_by_const(Arguments->type_hint)
			);
		}

		if (zend_binary_strcasecmp(ZSTR_VAL(Arguments->class_name), ZSTR_LEN(Arguments->class_name), "self", sizeof("self")- 1) == 0) {
			zend_error(E_ERROR, "%s::__inject() uses 'self' which is not allowed. It would create a loop.", ZSTR_VAL(Object->ce->name));
		}

		if (zend_binary_strcasecmp(ZSTR_VAL(Arguments->class_name), ZSTR_LEN(Arguments->class_name), "parent", sizeof("parent")- 1) == 0) {
			zend_error(E_ERROR, "%s::__inject() uses 'parent' which is not allowed. It would create a loop.", ZSTR_VAL(Object->ce->name));
		}

		SetPropertyToClass(Object, Arguments->name, Arguments->class_name);

		Arguments++;
	}
}

static zend_object *InjectionCreateClass(zend_class_entry *ce) {
	zend_object *RetVal;

	RetVal = zend_objects_new(ce);
	object_properties_init(RetVal, ce);

	zend_function *InjectFunction = GetFunction(RetVal, "__inject");
	ProcessInjectArguments(InjectFunction, RetVal);

	if (ce->parent) {
		zend_class_entry *ParentClass = ce->parent;
		while (ParentClass) {
			zend_object *ParentVal = zend_objects_new(ParentClass);
			zend_function *InjectFunction = GetFunction(ParentVal, "__inject");
			ProcessInjectArguments(InjectFunction, RetVal);

			ParentClass = ParentClass->parent;
		}
	}


	return RetVal;
}

static int RedCoreImplementInjectable(zend_class_entry *interface, zend_class_entry *class_type TSRMLS_DC) {
	if (class_type->create_object != NULL) {
		zend_error(E_ERROR, "%s interface can only be used on userland classes.", ZSTR_VAL(interface->name));
	}

	class_type->create_object = InjectionCreateClass;

	return SUCCESS;
}

void MINIT_InjectableInterface() {
	zend_class_entry ceInitInjectableInterface;
	INIT_NS_CLASS_ENTRY(ceInitInjectableInterface, NAMESPACE_CORE, "Injectable",
											RsCore_InjectableInterface_Functions)
	RsCore_InjectableInterface_ce = zend_register_internal_interface(&ceInitInjectableInterface);
	RsCore_InjectableInterface_ce->interface_gets_implemented = RedCoreImplementInjectable;
}
